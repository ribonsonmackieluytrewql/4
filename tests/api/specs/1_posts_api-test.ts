import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const posts = new PostsController();
const schemas = require('./data/schemas_testData_Posts.json');
//const schemas = require('./data/schemas_testData_like.json');

const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Posts controller`, () => {
    let postId: number;
 
    it(`should return 200 status code and all posts`, async () => {
        let response = await posts.getAllPosts();

         console.log("All Posts:");
         console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,4000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
     expect(response.body).to.be.jsonSchema(schemas.schema_allPosts); 
    //  expect(response.body).to.be.jsonSchema(schemas.schema_like); 
        
        postId = response.body[1].id;
    });

    it(`should return 404 error when getting post details with invalid postid`, async () => {
        let invalidPostId = 123133

        let response = await posts.getPostById(invalidPostId);
 
        checkStatusCode(response, 404);
        checkResponseTime(response,1000);
    });

    it(`should return 404 details when getting post details with valid id because there are not method to getting post by id`, async () => {
        let response = await posts.getAllPosts();
        let firstPostId: number = response.body[0].id;
        
        response = await posts.getPostById(firstPostId);
        
        checkStatusCode(response, 404);
        checkResponseTime(response,1000);

          console.log(response.body);
    });
});
