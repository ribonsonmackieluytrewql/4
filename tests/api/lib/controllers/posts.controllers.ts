import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
        .method("GET").
        url(`api/Posts`)
        .send();
        return response;
    }
    async CreatePost(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async getPostById(id) {
        const response = await new ApiRequest().prefixUrl(baseUrl).method("GET").url(`api/Posts/${id}`).send();
        return response;
    }
    async addPost(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async addReactionToPost(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
